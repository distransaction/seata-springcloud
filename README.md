# seata-springcloud

#### 介绍
通过seata实现分布式事务，微服务通过springcloud实现。

#### 软件架构
SpringCloud微服务架构，为实现事务一致性，通过seata以无侵入业务代码方式进行分布式事务处理。

#### 安装教程

1.  SpringCloud配置
    查看SpringBoot与SpringCloud版本对应关系：https://start.spring.io/actuator/info
    本项目采用SpringBoot：2.2.5.RELEASE，SpringCloud：Hoxton.SR10
    
2.  seata-server安装
    
    参考sql/seata.sql 如果是store.mode=file模式无须创建

    Docker安装：docker pull seataio/seata-server:1.2.0
    docker run -d --name seata-server  -e SEATA_IP=修改IP地址  -e SEATA_PORT=8091 -p 8091:8091 -v /usr/local/seata/:/seata-server/resources/ seataio/seata-server:1.2.0
    或者下载seata-server：https://github.com/seata/seata/releases/download/v1.2.0/seata-server-1.2.0.zip
    修改配置文件seata/conf/file.conf  用户名/密码：seata/seata
    修改配置文件seata/conf/registry.conf  registry.type = "eureka"
    
3.  provider安装
    
    参考sql/user.sql
    配置eureka-provider-user的yml文件，配置数据库连接和端口，以及jpa对应的hibernate方式
    配置eureka-provider-usercoupon的yml文件，配置数据库连接和端口，以及jpa对应的hibernate方式
    
4.  seata-client配置
    
    配置pom依赖包
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-alibaba-seata</artifactId>
        <version>2.2.0.RELEASE</version>
        <exclusions>
            <exclusion>
                <groupId>io.seata</groupId>
                <artifactId>seata-spring-boot-starter</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>io.seata</groupId>
        <artifactId>seata-spring-boot-starter</artifactId>
        <version>1.2.0</version>
    </dependency>
    
    配置DataSourceProxyConfig代理类
    拷贝file.conf.example和registry.conf 修改store.mode 如果是db则需要配置数据库连接 
    修改service.vgroup_mapping #注意vgroupMapping的修改(1.1.0以后)
    
    在每个库下面创建undo_log表sql/undo_log.sql
    
4.  seata相关配置
    spring:
      application:
        name: eureka-provider-user
      cloud:
          alibaba:
              seata:
                  tx-service-group: ${spring.application.name}-tx-group
    file.conf
    #注意vgroupMapping的修改(1.1.0以后)
    service {
      #vgroup->rgroup 参考spring.cloud.alibaba.seata.tx-service-group=eureka-provider-usercoupon-tx-group
      vgroupMapping.eureka-provider-user-tx-group = "default"
    }
    
5.  feign添加
    配置文件中添加
    #是否熔断处理
    feign:
      hystrix:
        enabled: true
    #解决客户端发现不了问题
    ribbon:
      eureka:
        enabled: true
    
#### 使用说明

1.  Fork 本仓库
    新建 Feat_xxx 分支
    git clone 分支
    or
    git clone https://gitee.com/distransaction/seata-springcloud.git
2.  创建数据库表
    执行sql/*.sql
    
    maven seata-springcloud(root) clean package
    
    启动eureka-server
    java -jar eureka-server-0.0.1.jar --spring.profiles.active=server1
    java -jar eureka-server-0.0.1.jar --spring.profiles.active=server2
    查看注册节点
    http://eurekaserver1:8761/
    
    启动seata-server
    ./seata-server.sh
    
    启动eureka-provider-usercoupon
    启动eureka-provider-user
    造数据 用户id对应金额为1万元
    http://localhost:8801/user/update?userId=1&userMoney=10000
    
3.  业务测试
    测试正常业务
    http://localhost:8801/user/testDTrans    
    测试回滚业务
    http://localhost:8801/user/testDTransFall

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
