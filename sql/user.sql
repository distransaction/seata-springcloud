#
# SQL Export
# Created by Querious (201046)
# Created: March 15, 2021 at 1:33:21 PM GMT+8
# Encoding: Unicode (UTF-8)
#


DROP DATABASE IF EXISTS `cs`;
CREATE DATABASE `cs` DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_0900_ai_ci;
USE `cs`;




DROP TABLE IF EXISTS `cs_xid_usercoupon`;
DROP TABLE IF EXISTS `cs_xid_user`;


CREATE TABLE `cs_xid_user` (
  `UserId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `UserMoney` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `cs_xid_user_UserId_Idx` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';


CREATE TABLE `cs_xid_usercoupon` (
  `UserCouponId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '优惠券编号',
  `CouponMoney` decimal(10,2) DEFAULT NULL COMMENT '优惠券金额',
  `UserId` bigint(20) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`UserCouponId`),
  KEY `fk_cs_xid_usercoupon_cs_xid_user` (`UserId`),
  KEY `cs_xid_usercoupon_UserCouponId_Idx` (`UserCouponId`),
  CONSTRAINT `fk_cs_xid_usercoupon_cs_xid_user` FOREIGN KEY (`UserId`) REFERENCES `cs_xid_user` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户优厚券';




TRUNCATE `cs_xid_user`;
INSERT INTO `cs_xid_user` VALUES (1,1000.0000);


TRUNCATE `cs_xid_usercoupon`;




