package com.bruce.eureka_provider_usercoupon.entity;

import lombok.*;

import javax.persistence.*;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:41 PM
 */
@Entity
@Table(name = "cs_xid_usercoupon")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserCoupon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userCouponId;
    private long userId;
    private double couponMoney;
}
