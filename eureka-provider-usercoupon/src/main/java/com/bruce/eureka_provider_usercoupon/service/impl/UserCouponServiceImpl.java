package com.bruce.eureka_provider_usercoupon.service.impl;

import com.bruce.eureka_provider_usercoupon.entity.UserCoupon;
import com.bruce.eureka_provider_usercoupon.repository.UserCouponRepository;
import com.bruce.eureka_provider_usercoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:45 PM
 */
@Service
public class UserCouponServiceImpl implements UserCouponService {

    @Autowired
    private UserCouponRepository userCouponRepository;

    @Transactional
    @Override
    public UserCoupon save(UserCoupon userCoupon) {
        return userCouponRepository.save(userCoupon);
    }

    @Override
    public UserCoupon findByUserCouponId(long id) {
        return userCouponRepository.findByUserCouponId(id);
    }
}
