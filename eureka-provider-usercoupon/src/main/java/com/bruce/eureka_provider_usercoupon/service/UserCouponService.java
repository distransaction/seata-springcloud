package com.bruce.eureka_provider_usercoupon.service;

import com.bruce.eureka_provider_usercoupon.entity.UserCoupon;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:44 PM
 */
public interface UserCouponService {
    UserCoupon save(UserCoupon userCoupon);
    UserCoupon findByUserCouponId(long id);
}
