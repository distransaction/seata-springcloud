package com.bruce.eureka_provider_usercoupon.controller;

import com.bruce.eureka_provider_usercoupon.entity.UserCoupon;
import com.bruce.eureka_provider_usercoupon.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:47 PM
 */
@RestController
@RequestMapping("/usercoupon")
public class UserCouponController {
    @Autowired
    private UserCouponService userCouponService;


    //http://localhost:8802/usercoupon/update?userId=1&userCouponId=1&couponMoney=10
    @GetMapping(value = "/update")
    public UserCoupon update(long userId, long userCouponId, double couponMoney) {
        UserCoupon userCoupon = userCouponService.findByUserCouponId(userCouponId);
        if(userCoupon == null){
            userCoupon = new UserCoupon();
        }
        userCoupon.setUserId(userId);
        userCoupon.setCouponMoney(couponMoney);

        return userCouponService.save(userCoupon);
    }

    //http://localhost:8802/usercoupon/findByUserCouponId?id=1
    @GetMapping(value = "/findByUserCouponId")
    public UserCoupon findByUserCouponId(long id) {
        return userCouponService.findByUserCouponId(id);
    }

}
