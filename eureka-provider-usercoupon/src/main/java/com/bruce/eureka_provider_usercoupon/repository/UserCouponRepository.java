package com.bruce.eureka_provider_usercoupon.repository;

import com.bruce.eureka_provider_usercoupon.entity.UserCoupon;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:43 PM
 */
public interface UserCouponRepository extends JpaRepository<UserCoupon, Long> {
    UserCoupon findByUserCouponId(long userCouponId);
}

