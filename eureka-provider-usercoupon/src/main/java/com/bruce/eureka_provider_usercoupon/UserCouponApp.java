package com.bruce.eureka_provider_usercoupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 1:40 PM
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
public class UserCouponApp {
    public static void main(String[] args) {
        SpringApplication.run(UserCouponApp.class, args);
    }
}
