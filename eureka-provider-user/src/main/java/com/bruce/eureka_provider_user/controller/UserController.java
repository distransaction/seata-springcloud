package com.bruce.eureka_provider_user.controller;

import com.bruce.eureka_provider_user.entity.User;
import com.bruce.eureka_provider_user.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 11:29 AM
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    //http://localhost:8801/user/update?userId=1&userMoney=10000
    @GetMapping(value = "/update")
    public User update(long userId, double userMoney) {
        User userInfo = userService.findByUserId(userId);
        if(userInfo == null){
            userInfo = new User();
        }
        userInfo.setUserMoney(userMoney);

        return userService.save(userInfo);
    }

    //http://localhost:8801/user/findByUserId?id=1
    @GetMapping(value = "/findByUserId")
    public User findByUserId(long id) {
        return userService.findByUserId(id);
    }

    //测试正常业务
    //http://localhost:8801/user/testDTrans
    @GetMapping(value = "/testDTrans")
    public boolean testDTrans(){
        return userService.testDTrans();
    }

    //测试回滚业务
    //http://localhost:8801/user/testDTransFall
    @GetMapping(value = "/testDTransFall")
    public boolean testDTransFall(){
        return userService.testDTransFall();
    }

}
