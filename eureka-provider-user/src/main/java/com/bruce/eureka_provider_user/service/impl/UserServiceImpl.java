package com.bruce.eureka_provider_user.service.impl;

import com.bruce.eureka_provider_user.entity.User;
import com.bruce.eureka_provider_user.feign.FeignUserCoupon;
import com.bruce.eureka_provider_user.repository.UserRepository;
import com.bruce.eureka_provider_user.service.UserService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 11:26 AM
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FeignUserCoupon userCoupon;

    /**
     * 更新用户信息
     * @param userInfo
     * @return
     */
    @Override
    @Transactional
    public User save(User userInfo) {
        User user = userRepository.save(userInfo);
        return user;
    }

    /**
     * 获取用户根据用户ID
     *
     * @param id
     * @return
     */
    @Override
    public User findByUserId(long id) {
        return userRepository.findByUserId(id);
    }

    @GlobalTransactional
    @Override
    public boolean testDTrans() {
        log.info("------->交易开始");
        String xid = RootContext.getXID();
        log.info(String.format("------->xid:%s", xid));

        User user = findByUserId(1);
        log.info(String.format("------->userId:%d, userMondy:%f", user.getUserId(), user.getUserMoney()));
        //减去消费金额
        double consumptionMoney = 100;
        user.setUserMoney(user.getUserMoney()-consumptionMoney);
        save(user);

        log.info(String.format("------->B-userId:%d, userMondy:%f", user.getUserId(), user.getUserMoney()));
        if(user.getUserMoney()<0){
            throw new RuntimeException("用户帐户余额不足异常，应提前判断，写在此为了测试回滚");
        }
        //发一张消费金额1/10的优惠券
        Object updateVal = userCoupon.update(user.getUserId(), 0, consumptionMoney*0.1);
        if(updateVal.equals("error")){
            log.error("------->发放优惠券异常");
            throw new RuntimeException("发放优惠券异常");
        }
        log.info("------->suc");
        return true;
    }

    @GlobalTransactional
    @Override
    public boolean testDTransFall() {
        log.info("------->交易开始");
        String xid = RootContext.getXID();
        log.info(String.format("------->xid:%s", xid));

        User user = findByUserId(1);
        log.info(String.format("------->A-userId:%d, userMondy:%f", user.getUserId(), user.getUserMoney()));
        //减去消费金额
        double consumptionMoney = 100;
        user.setUserMoney(user.getUserMoney()-consumptionMoney);
        save(user);

        log.info(String.format("------->B-userId:%d, userMondy:%f", user.getUserId(), user.getUserMoney()));
        if(user.getUserMoney()<0){
            throw new RuntimeException("用户帐户余额不足异常，应提前判断，写在此为了测试回滚");
        }

        //发一张消费金额1/10的优惠券
        Object updateVal = userCoupon.update(user.getUserId(), 0, consumptionMoney*0.1);
        log.info(updateVal.toString());

        //人为异常回滚，此时已减消费金额，并且已经发放消费券，可以在此设置断点，查看数据库
        if(!updateVal.equals("")){
            log.error("------->发放优惠券异常");
            throw new RuntimeException("发放优惠券异常");
        }
        log.info("------->suc");
        return true;
    }
}
