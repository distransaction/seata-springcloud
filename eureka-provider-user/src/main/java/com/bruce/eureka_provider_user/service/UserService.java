package com.bruce.eureka_provider_user.service;

import com.bruce.eureka_provider_user.entity.User;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 11:26 AM
 */
public interface UserService {
    User save(User userInfo);
    User findByUserId(long id);
    boolean testDTrans();
    boolean testDTransFall();
}
