package com.bruce.eureka_provider_user.repository;

import com.bruce.eureka_provider_user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 11:24 AM
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserId(long userId);
}


