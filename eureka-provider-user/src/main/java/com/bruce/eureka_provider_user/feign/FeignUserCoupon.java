package com.bruce.eureka_provider_user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/16 5:44 PM
 */
@FeignClient(value = "eureka-provider-usercoupon", fallback = FeignUserCouponFall.class)
public interface FeignUserCoupon {
    @GetMapping("/usercoupon/update")
    Object update(@RequestParam("userId") long userId, @RequestParam("userCouponId") long userCouponId, @RequestParam("couponMoney") double couponMoney);
}
