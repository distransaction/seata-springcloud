package com.bruce.eureka_provider_user.feign;

import org.springframework.stereotype.Component;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/17 3:43 PM
 */
@Component
public class FeignUserCouponFall implements FeignUserCoupon {
    @Override
    public Object update(long userId, long userCouponId, double couponMoney) {
        return "error";
    }
}
