package com.bruce.eureka_provider_user.entity;

import lombok.*;

import javax.persistence.*;

/**
 * 类注释
 *
 * @author Bruce
 * @date 2021/3/15 11:22 AM
 */
@Entity
@Table(name = "cs_xid_user")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    private double userMoney;
}
