package com.bruce.eureka_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Eureka注册中心
 * 启动：
 * java -jar eureka-server-0.0.1.jar --spring.profiles.active=server1
 * java -jar eureka-server-0.0.1.jar --spring.profiles.active=server2
 *
 * @author Bruce
 * @date 2021/3/12 5:33 PM
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApp.class, args);
    }
}
